package MainTasks.TeaParty;

import org.junit.Test;

import static org.junit.Assert.*;
import static MainTasks.TeaParty.PartyDataProcessing.outcomeCalc;

public class PartyDataProcessingTest {

    @Test
    public void testLess5Candy() {
        assertEquals(0, outcomeCalc(4, 5));
    }

    @Test
    public void testLess5Tea() {
        assertEquals(0, outcomeCalc(5, 4));
    }

    @Test
    public void testEqualValues() {
        assertEquals(1, outcomeCalc(5, 5));
    }

    @Test
    public void testDoubleTea() {
        assertEquals(2, outcomeCalc(5, 10));
    }

    @Test
    public void testDoubleCandy() {
        assertEquals(2, outcomeCalc(10, 5));
    }

    @Test
    public void testTripleCandy() {
        assertEquals(2, outcomeCalc(30, 10));
    }

    @Test
    public void testTripleTea() {
        assertEquals(2, outcomeCalc(10, 30));
    }

    @Test
    public void testZeroCandy() {
        assertEquals(0, outcomeCalc(0, 10));
    }

    @Test
    public void testZeroTea() {
        assertEquals(0, outcomeCalc(10, 0));
    }
}