package MainTasks.FizzBuzzString;

import static org.junit.Assert.*;
import static MainTasks.FizzBuzzString.FizzBuzzDataProcessing.fizzBuzz;

import org.junit.Test;

public class FizzBuzzDataProcessingTest {

    @Test
    public void testEmptyString() {
        assertEquals("", fizzBuzz(""));
    }

    @Test
    public void testfString() {
        assertEquals("Fizz", fizzBuzz("fstring"));

    }

    @Test
    public void testFstring() {
        assertEquals("Fizz", fizzBuzz("Fstring"));

    }

    @Test
    public void testbString() {
        assertEquals("Buzz", fizzBuzz("stringb"));
    }

    @Test
    public void testBstring() {
        assertEquals("Buzz", fizzBuzz("stringB"));
    }

    @Test
    public void testfbString() {
        assertEquals("FizzBuzz", fizzBuzz("fstringb"));
    }

    @Test
    public void testFBString() {
        assertEquals("FizzBuzz", fizzBuzz("FstringB"));
    }

    @Test
    public void testFbString() {
        assertEquals("FizzBuzz", fizzBuzz("Fstringb"));
    }

    @Test
    public void testfBString() {
        assertEquals("FizzBuzz", fizzBuzz("fstringB"));
    }

}