package MainTasks.Alarm;

import org.junit.Test;

import static org.junit.Assert.*;
import static MainTasks.Alarm.AlarmDataProcessing.alarmTime;

public class AlarmDataProcessingTest {

    @Test
    public void testVacSun() {
        assertEquals("Alarm off", alarmTime(0, true));
    }


    @Test
    public void TestVacWeekDay() {
        assertEquals("Alarm time: 10:00", alarmTime(1, true));
    }

    @Test
    public void TestNotVacSun() {
        assertEquals("Alarm time: 10:00", alarmTime(0, false));
    }

    @Test
    public void TestNotVacWeekDay() {
        assertEquals("Alarm time: 07:00", alarmTime(1, false));
    }
}