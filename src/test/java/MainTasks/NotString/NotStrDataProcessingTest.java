package MainTasks.NotString;

import org.junit.Test;

import static org.junit.Assert.*;
import static MainTasks.NotString.NotStrDataProcessing.notCheck;

public class NotStrDataProcessingTest {

    @Test
    public void testEmptyString() {
        assertEquals("not", notCheck(""));
    }

    @Test
    public void testNotredameString() {
        assertEquals("Notredame", notCheck("Notredame"));
    }

    @Test
    public void testnotString() {
        assertEquals("notstring", notCheck("notstring"));
    }

    @Test
    public void testStringWOnot() {
        assertEquals("notsome string here", notCheck("some string here"));
    }

    @Test
    public void testNumericString() {
        assertEquals("not1234567890", notCheck("1234567890"));
    }

    @Test
    public void testSpecialsString() {
        assertEquals("not/*-+][}{)(", notCheck("/*-+][}{)("));
    }

}