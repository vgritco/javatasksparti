package AdditionalTasks.StrSimilarities;

import org.junit.Test;

import static AdditionalTasks.StrSimilarities.SimilaritiesDataProcessing.similaritiesSumM1;
import static org.junit.Assert.assertEquals;

public class SimilaritiesDataProcessingTest {

    private StringBuilder sb = new StringBuilder();

    @Test
    public void testUsualString() {
        sb = new StringBuilder("ababaa");
        assertEquals(11, similaritiesSumM1(sb));

    }

    @Test
    public void testEmptyString() {
        sb = new StringBuilder();
        assertEquals("Here is Empty String test result: ", 0, similaritiesSumM1(sb));
    }

    @Test
    public void testLongString() {
        sb = new StringBuilder("fgsdfklgjsdflgjklsdfgjlsdfjglsdfjglsdfjglsdfjgksdg");
        assertEquals(58, similaritiesSumM1(sb));
    }

    @Test
    public void testNumberString() {
        sb = new StringBuilder("1564645645646312123");
        assertEquals(21, similaritiesSumM1(sb));
    }

    @Test
    public void testSpecialChars() {
        sb = new StringBuilder("/*-+][;'./");
        assertEquals(11, similaritiesSumM1(sb));

    }

    @Test
    public void testStingsSpace() {
        sb = new StringBuilder("fgsdf klgjsdf lgjklsd fgjlsdfjg lsdfjgls dfjglsdf jgksdg");
        assertEquals(64, similaritiesSumM1(sb));
    }

    @Test
    public void testCombinedString() {
        sb = new StringBuilder("fgsdf klgjsdf lg/*-+sd fg54564676g ls_)(_gls dfjglsdf jgksdg");
        assertEquals(66, similaritiesSumM1(sb));
    }

    @Test
    public void testOnlySpaceString() {
        sb = new StringBuilder("     ");
        assertEquals(15, similaritiesSumM1(sb));
    }

    @Test
    public void testFloatNumber() {
        sb = new StringBuilder("5.25 2.28 14.88 3.32 0.0");
        assertEquals(25, similaritiesSumM1(sb));
    }

}
