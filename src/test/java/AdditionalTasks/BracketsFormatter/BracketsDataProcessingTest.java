package AdditionalTasks.BracketsFormatter;

import org.junit.Test;
import static org.junit.Assert.*;
import static AdditionalTasks.BracketsFormatter.BracketsDataProcessing.balanceBrackets;

public class BracketsDataProcessingTest {

    @Test
    public void testEmptyString(){
        assertEquals("NO", balanceBrackets(""));
    }

    @Test ///
    public void testNumericString(){
        assertEquals("NO", balanceBrackets("1234567890"));
    }

    @Test
    public void testAlphabeticalString(){
        assertEquals("NO", balanceBrackets("abdsflsjgklsdfgjkdfsgsdfk"));
    }

    @Test
    public void testBracketsString(){
        assertEquals("NO", balanceBrackets("(([{)()]})"));
    }

    @Test
    public void testCombinedString(){
        assertEquals("NO", balanceBrackets("(i.e., (, [, or {) occurs to the left of a closing bracket (i.e., ), ], or })"));
    }

    @Test
    public void testGoodBalanceString(){
        assertEquals("YES", balanceBrackets("(([{()}]))"));
    }
}