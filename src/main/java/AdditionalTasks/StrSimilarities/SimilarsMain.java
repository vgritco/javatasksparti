/*
Task:
For two strings A and B, we define the similarity of the strings to be the length of the longest prefix common
to both strings. For example, the similarity of strings "abc" and "abd" is 2,
while the similarity of strings "aaa" and "aaab" is 3.
Calculate the sum of similarities of a string S with each of it's suffixes.

Example:
BracketsInput: ababaa; Output: 11

Explanation:
The suffixes of the string are "ababaa", "babaa", "abaa", "baa", "aa" and "a".
The similarities of these strings with the string "ababaa" are 6,0,3,0,1, & 1 respectively.
Thus, the answer is 6 + 0 + 3 + 0 + 1 + 1 = 11.
 */

package AdditionalTasks.StrSimilarities;

public class SimilarsMain {

    public static void main(String[] args) {

        StringBuilder sb = new StringBuilder(SimilaritiesInput.inputString());

        System.out.println("Similarities by method one: " + SimilaritiesDataProcessing.similaritiesSumM1(sb));
        //System.out.println("Similarities by method two: " + BracketsDataProcessing.similaritiesSumM2(sb));

    } // main end
}    // class end

