package AdditionalTasks.StrSimilarities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SimilaritiesInput {


    /**
     * Read string from standard input
     *
     * @return StringBuilder str - entered string
     */
    static StringBuilder inputString(){

        StringBuilder str = new StringBuilder();

        try {
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);

            System.out.print("Enter string to calculate similarities: ");
            str.append(br.readLine());

            isr.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return str;
    }
}
