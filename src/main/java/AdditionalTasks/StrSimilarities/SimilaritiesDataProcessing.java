package AdditionalTasks.StrSimilarities;

public class SimilaritiesDataProcessing {

    /**
     * Calculate the similarities in the given string
     *
     * @param sb The string in which are counted similarities
     * @return int sum - number of the similarities
     */
    static int similaritiesSumM1(StringBuilder sb) {

        int i = 0, sum = 0;
        String copy_sb = new String(sb);

        while (sb.length() > 0) {
            if (i < sb.length() && sb.charAt(i) == copy_sb.charAt(i)) {
                sum++;
            } else {
                sb.deleteCharAt(0);
                i = 0;
                continue;
            } // if(...) end
            i++;
        } // while(...) end
        return sum;
    } // similaritiesSumM1 end


    /**
     * Calculate the similarities in the given string
     *
     * @param sb The string in which are counted similarities
     * @return int sum - number of the similarities
     */
    static int similaritiesSumM2(StringBuilder sb) {

        int i = 0, sum = 0;
        String copy_sb = new String(sb);

        while (true) {
            if (i < sb.length() && sb.charAt(i) == copy_sb.charAt(i)) {
                sum++;
            } else {
                sb.deleteCharAt(0);
                i = 0;
                if (sb.length() == 0) break;
                continue;
            } // if(...) end
            i++;
        } // while(...) end
        return sum;
    } // similaritiesSumM2 end
} // BracketsDataProcessing class end
