// example for testing: (i.e., (, [, or {) occurs to the left of a closing bracket (i.e., ), ], or })

/*
A bracket is considered to be any one of the following characters: (, ), {, }, [, or ].
Two brackets are considered to be a matched pair if the an opening bracket
(i.e., (, [, or {) occurs to the left of a closing bracket (i.e., ), ], or })
of the exact same type. There are three types of matched pairs of brackets: [], {}, and ().
A matching pair of brackets is not balanced if the set of brackets it encloses are not matched.
For example, {[(])}is not balanced because the contents in between { and } are not balanced.
The pair of square brackets encloses a single, unbalanced opening bracket, (,
and the pair of parentheses encloses a single, unbalanced closing square bracket, ].
By this logic, we say a sequence of brackets is considered to be balanced if the following conditions are met:
â€¢	It contains no unmatched brackets.
â€¢	The subset of brackets enclosed within the confines of a matched pair of brackets is also a matched pair of brackets.
Given  string of brackets, determine whether sequence of brackets is balanced.
If a string is balanced, print YES on a new line; otherwise, print NO on a new line.
 */

package AdditionalTasks.BracketsFormatter;

public class BracketsMain {

    public static void main(String[] args) {

        String str = BracketsInput.inputString(), answer;

        answer = BracketsDataProcessing.balanceBrackets(str);
        System.out.println("Are they balanced ? The answer is:");

        System.out.println(answer);
    }
}