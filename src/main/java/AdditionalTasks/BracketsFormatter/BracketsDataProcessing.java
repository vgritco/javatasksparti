package AdditionalTasks.BracketsFormatter;

import java.util.Stack;

public class BracketsDataProcessing {

    /**
     * Checks if brackets in the given string are balanced
     *
     * @param str string to check brackets balance
     * @return String "YES" is brackes are balanced, String "NO" if they aren't
     */
    public static String balanceBrackets(String str){

        char myChar;
        Stack<Character> st = new Stack<>();

        // Remove from string all elements except of brackets of the three types
        str = str.replaceAll("[^(){}\\]\\[]+", "");
        if(str.isEmpty()){
            System.out.println("String doesn't contains brackets");
            return "NO";
        } // if(...) end

        System.out.println("Brackets from entered string: " + str);

        for (int i = 0; i < str.length(); i++) {
            if (str.length() % 2 != 0) {
                return "NO";
            } // if(...) end

            myChar = str.charAt(i);

            // If char is opening bracket - insert in the stack
            if (myChar == '(' || myChar == '[' || myChar == '{') {
                st.push(myChar); // insert in stack
                continue;
            } // if(...) end

            // If char is closing bracket - start checking for brackets balance
            if (myChar == ')' || myChar == ']' || myChar == '}') {
                // If stack is empty, then closing bracket doesn't have the opening pair
                if (st.empty()) {
                    return "NO";
                } else {
                    char check = st.pop(); // Remove from stack and check. Can be replaced by .peek()
                    // but then .pop() will be required in if(...) body
                    if (myChar == ')' && check == '('
                            || myChar == ']' && check == '['
                            || myChar == '}' && check == '{') {
                        continue;
                    } else {
                        return "NO";
                    } // else end
                } // else end
            } // if(...)  end
        } // for(...) end

        // If stack contains any elements after finishing cycle - brackets aren't balanced
        if (!st.empty()) {
            return "NO";
        } // if(...) end
        return "YES";

    } // balanceBrackets(...) end
} // class BracketsDataProcessing end