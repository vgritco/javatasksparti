package AdditionalTasks.BracketsFormatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BracketsInput {

    public static String inputString(){

        String str = "";


        try {

            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);

            System.out.println("Enter some string: ");
            str = br.readLine();

            isr.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return str;
    }
}