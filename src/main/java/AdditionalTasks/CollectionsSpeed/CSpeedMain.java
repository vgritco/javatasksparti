package AdditionalTasks.CollectionsSpeed;

import static AdditionalTasks.CollectionsSpeed.FillingCollections.*;

public class CSpeedMain {
    public static void main(String[] args) {

        fillAll();
        //printAll();
        linkedListSortTime();
        arrayListSortTime();
        vectorSortTime();
        hashSetSortTime();
        linkedHashSetSortTime();
    }
}
