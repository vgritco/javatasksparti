package AdditionalTasks.CollectionsSpeed;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

class FillingCollections {

    private static final int COLLECTION_SIZE = 10;
    private static double startTime;
    private static double duration;
    private static BigDecimal durationDB;
    private static NumberFormat sec = new DecimalFormat();


    // Collections declaration
    private static LinkedList<Integer> linkedList = new LinkedList<>();
    private static ArrayList<Integer> arrayList = new ArrayList<>();
    private static Vector<Integer> vector = new Vector<>();
    private static PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
    private static HashSet<Integer> hashSet = new HashSet<>();
    private static LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();
    private static HashMap<Integer, Integer> hashMap = new HashMap<>();
    private static LinkedHashMap<Integer, Integer> linkedHashMap = new LinkedHashMap<>();
    private static WeakHashMap<Integer, Integer> weakHashMap = new WeakHashMap<>();
    private static Hashtable<Integer, Integer> hashtable = new Hashtable<>();
    private static TreeMap<Integer, Integer> treeMap = new TreeMap<>();

    private static Integer[] fillArray(Integer amount) {

        Integer[] array = new Integer[amount];
        Random rand = new Random();
        for (Integer i = 0; i < amount; i++) {
            array[i] = rand.nextInt(50) + 1;
        }

        return array;

    }

    private static <T> void fillCollectionFromArray(T[] array, Collection<T> c) {
        c.addAll(Arrays.asList(array));
    }

    static void fillAll() {

        // Linked List
        fillCollectionFromArray(fillArray(COLLECTION_SIZE), linkedList);

        // ArrayList
        fillCollectionFromArray(fillArray(COLLECTION_SIZE), arrayList);

        // Vector
        fillCollectionFromArray(fillArray(COLLECTION_SIZE), vector);

        // PriorityQueue
        fillCollectionFromArray(fillArray(COLLECTION_SIZE), priorityQueue);

        // HashSet
        fillCollectionFromArray(fillArray(COLLECTION_SIZE), hashSet);

        // LinkedHashSet
        fillCollectionFromArray(fillArray(COLLECTION_SIZE), linkedHashSet);


        // Maps

        // HashMap
        //mapPopulatorGeneric(hashMap, (byte) 0);
        fillMap(hashMap, COLLECTION_SIZE);

        // LinkedHashMap
        fillMap(linkedHashMap, COLLECTION_SIZE);

        // WeakHashMap
        fillMap(weakHashMap, COLLECTION_SIZE);

        // Hashtable
        fillMap(hashtable, COLLECTION_SIZE);

        // TreeMap
        fillMap(treeMap, COLLECTION_SIZE);
    }

    static void printAll() {
        // Print collections
        System.out.println("Linked List:\n" + Arrays.toString(linkedList.toArray()));

        System.out.println("\nArray List:\n" + Arrays.toString(arrayList.toArray()));

        System.out.println("\nVector:\n" + Arrays.toString(vector.toArray()));

        System.out.println("\nPriority Queue:\n" + Arrays.toString((priorityQueue.toArray())));

        System.out.println("\nHash Set:\n" + Arrays.toString(hashSet.toArray()));

        System.out.println("\nLinked Hash Set:\n" + Arrays.toString(linkedHashSet.toArray()));

        System.out.println("\nHash Map Keys and Values:\n" + hashMap.keySet().toString() + '\n' + hashMap.values().toString());

        System.out.println("\nLinked Hash Map Keys and Values:\n" + linkedHashMap.keySet().toString() + '\n' + linkedHashMap.values().toString());

        System.out.println("\nWeak Map Keys and Values:\n" + weakHashMap.keySet().toString() + '\n' + weakHashMap.values().toString());

        System.out.println("\nHashtable Keys and Values:\n" + hashtable.keySet().toString() + '\n' + hashtable.values().toString());

        System.out.println("\nTree Map Keys and Values:\n" + treeMap.keySet().toString() + '\n' + treeMap.values().toString());

    }

    private static void fillMap(Map<Integer, Integer> map, int size) {

        Random rand = new Random();
        for (int i = 0; i < size; i++) {
            map.put(i + 1, rand.nextInt(50) + 1);
        }
    }

    static void linkedListSortTime() {
        sec.setMaximumFractionDigits(25);
        startTime = System.nanoTime();
        Collections.sort(linkedList);
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationDB = BigDecimal.valueOf(duration);

        System.out.println("Time elapsed to sort LinkedList: " + sec.format(durationDB));
    }

    static void arrayListSortTime() {
        sec.setMaximumFractionDigits(25);
        startTime = System.nanoTime();
        Collections.sort(arrayList);
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationDB = BigDecimal.valueOf(duration);

        System.out.println("Time elapsed to sort ArrayList: " + sec.format(durationDB));
    }

    static void vectorSortTime() {
        sec.setMaximumFractionDigits(25);
        startTime = System.nanoTime();
        Collections.sort(vector);
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationDB = BigDecimal.valueOf(duration);

        System.out.println("Time elapsed to sort Vector: " + sec.format(durationDB));
    }

   static void hashSetSortTime() {
        sec.setMaximumFractionDigits(25);
        startTime = System.nanoTime();
        TreeSet treeSet = new TreeSet();
        treeSet.addAll(hashSet);
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationDB = BigDecimal.valueOf(duration);

        System.out.println("Time elapsed to sort HashSet: " + sec.format(durationDB));
    }

    static void linkedHashSetSortTime() {
        sec.setMaximumFractionDigits(25);
        startTime = System.nanoTime();
        TreeSet treeSet = new TreeSet();
        treeSet.addAll(hashSet);
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationDB = BigDecimal.valueOf(duration);

        System.out.println("Time elapsed to sort LinkedHashSet: " + sec.format(durationDB));

    }
}
