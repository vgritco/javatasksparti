package MainTasks.NotString;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NotStrInput {

    public static String inputString() {

        String str = "";
        try {

            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);

            System.out.print("Enter some string: ");
            str = br.readLine();

            isr.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return str;
    }
}

