package MainTasks.NotString;

public class NotStrDataProcessing {

    /**
     * Checks if string starts with "N/not and add "not" in the begining of the string, if it's not
     *
     * @param str String to check
     * @return String itself, if already starts with "not", or modified string with "not" in the beginning
     */
    public static String notCheck(String str){

        if (!str.matches("^[Nn]ot.*")) {
            str = "not" + str;
       } // if(...) end

        return str;
    } // notCheck(...) end
} // class NotStrDataProcessing end
