/*
Given a string, return a new string where "not " has been added to the front.
However, if the string already begins with "not", return the string unchanged.
Note: use .equals() to compare 2 strings.
 */

package MainTasks.NotString;

public class NotStrMain {

    public static void main(String[] args) {

        String str = NotStrInput.inputString();
        System.out.println(NotStrDataProcessing.notCheck(str));
    }
}
