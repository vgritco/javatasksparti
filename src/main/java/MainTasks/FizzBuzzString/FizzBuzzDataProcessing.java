package MainTasks.FizzBuzzString;

public class FizzBuzzDataProcessing {

    /**
     * Checks string's first and last chars and return string value respectively
     *
     * @param str String value, which represents string to check
     * @return string itself, or modified string in case if some changes were made
     */
    public static String fizzBuzz(String str) {

        String sum = "";

        if(str.startsWith("f") || str.startsWith("F")) sum += "Fizz";
        if(str.endsWith("b") || str.endsWith("B")) sum+= "Buzz";

        if(sum.isEmpty()) return str;
        else return sum;
    } // fizzBuzz(...) end
} // class FizzBuzzDataProcessing end
