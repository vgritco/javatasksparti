/*
2.	Given a string str, if the string starts with "f" return "Fizz".
If the string ends with "b" return "Buzz". If both the "f" and "b" conditions are true, return "FizzBuzz".
In all other cases, return the string unchanged. Read string from console.
 */

package MainTasks.FizzBuzzString;

class FizzBuzzMain {

    public static void main(String[] args) {

        String str = FizzInput.inputString();
        System.out.println("String: " + FizzBuzzDataProcessing.fizzBuzz(str));

    } // main()
} // class FizzBuzzMain