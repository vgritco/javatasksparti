package MainTasks.TeaParty;

public class PartyDataProcessing {

    /**
     * Сalculates the party outcome based on amount of candy and tea
     *
     * @param candy amount of candy
     * @param tea amount of tea
     * @return outcome of the party as an integer value
     */
    static int outcomeCalc(int candy, int tea) {
        int outcome;

        if (candy >= 5 && tea >= 5) {
            if (candy >= tea * 2 || tea >= candy * 2) {
                outcome = 2;
            } else outcome = 1;
        } else outcome = 0;

        return outcome;
    } // outcomeCalc(...) end

    /**
     * Prints party feedback based on outcome value
     *
     * @param outcome integer which presents party's outcome
     */
    static void outcomePrint(int outcome) {

        String soutcome = outcome == 0 ? "Bad Party :-(" : outcome == 1 ? "Good Party :-|" : "Great Party! :-)";
        System.out.println(soutcome);
    } // outcomePrint(...) end
} // class PartyDataProcessing end
