package MainTasks.TeaParty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/*
 Java can't return two values from one method, so we create another class for this purpose
 */
public class PartyInput {

    private int candy;
    private int tea;

    public PartyInput() {}

    public PartyInput(int candy, int tea) {
        this.candy = candy;
        this.tea = tea;
    }

    public int getCandy() {
        return candy;
    }

    public int getTea() {
        return tea;
    }

    void setCandy(int candy) {
        this.candy = candy;
    }

    void setTea(int tea) {
        this.tea = tea;
    }

    public void input() {

        try {

            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);

            System.out.print("Enter amount of candy: ");
            this.candy = Integer.parseInt(br.readLine());

            System.out.print("Enter amount of tea: ");
            this.tea = Integer.parseInt(br.readLine());

            isr.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


}
