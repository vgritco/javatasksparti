/*
 We are having a party with amounts of tea and candy.
 Return the int outcome of the party encoded as 0=bad, 1=good, or 2=great.
 A party is good (1) if both tea and candy are at least 5.
 However, if either tea or candy is at least double the amount of the other one, the party is great (2).
 However, in all cases, if either tea or candy is less than 5, the party is always bad (0).
 Read values of tea and candy from console.
 */

package MainTasks.TeaParty;

import java.util.Stack;

class PartyMain {

    public static void main(String[] args) {
        int candy, tea, outcome;

        PartyInput sweeties = new PartyInput();
        sweeties.input();

        candy = sweeties.getCandy();
        tea = sweeties.getTea();

        outcome = PartyDataProcessing.outcomeCalc(candy, tea);
        PartyDataProcessing.outcomePrint(outcome);

    } // main()
} // class AlarmMain