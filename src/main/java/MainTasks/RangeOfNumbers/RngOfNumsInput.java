package MainTasks.RangeOfNumbers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/*
 Java can't return two values from one method, so we create another class for this purpose
 */
public class RngOfNumsInput {

    private int start;
    private int end;

    public RngOfNumsInput() {}

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    void setStart(int start) {
        this.start = start;
    }

    void setEnd(int end) {
        this.end = end;
    }

    public void input() {

        try {

            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);

            System.out.print("Enter start number: ");
            start = Integer.parseInt(br.readLine());

            while (true) {
                System.out.print("Enter the end value: ");
                end = Integer.parseInt(br.readLine());

                if (end < start) {
                    System.out.println("End should be equal or greater than start");
                } else break;
            }

            isr.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


}
