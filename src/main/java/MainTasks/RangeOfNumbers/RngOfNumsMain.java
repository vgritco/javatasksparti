/*
Given start and end numbers, return a new array containing the sequence of integers from start up to but not including end,
so start=5 and end=10 yields {5, 6, 7, 8, 9}.
The end number will be greater or equal to the start number.
Note that a length-0 array is valid. Read start and end from console.
 */

package MainTasks.RangeOfNumbers;

import java.util.Arrays;

public class RngOfNumsMain {

    public static void main(String[] args) {

        int start, end;

        RngOfNumsInput range = new RngOfNumsInput();
        range.input();

        start = range.getStart();
        end = range.getEnd();

        System.out.println(Arrays.toString(RngOfNumsDataProcessing.rangeCalc(start, end)));
    }
}
