package MainTasks.RangeOfNumbers;

public class RngOfNumsDataProcessing {

    /**
     * Creating array which contains numbers from the given range
     * start value is included, end value is excluded
     *
     * @param start first value of the range
     * @param end last+1 value of the range
     * @return array of integers
     */
    public static int[] rangeCalc(int start, int end) {

        int arr_size = end - start;
        int[] numbers = new int[arr_size];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = start + i;
        } // if(...) end

        return numbers;

    } // rangeCalc(...) end
} // class RngOfNumsDataProcessing end
