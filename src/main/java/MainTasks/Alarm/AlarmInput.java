package MainTasks.Alarm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;


/*
 Java can't return two values from one method, so we create another class for this purpose
 */
public class AlarmInput {

    private int day;
    private boolean isVacation;

    public AlarmInput() {}

    public int getDay() {
        return day;
    }

    public boolean getIsVacation() {
        return isVacation;
    }

    void setDay(int day) {
        this.day = day;
    }

    void setIsVacation(boolean isVacation) {
        this.isVacation = isVacation;
    }

    public void input() {

        String[] Days = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

        System.out.println("Week Days:\n" + Arrays.toString(Days));

        try {
            InputStreamReader isr = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(isr);

            while (true) {
                System.out.print("Select the day of the week\nAs number from 0 to 6 inclusive: ");
                day = Integer.parseInt(br.readLine());
                if (day < 0 || day > 6) {
                    System.out.println("Please enter valid Day number");
                } else break;
            }


            System.out.println("Is it a vacation?\n1 - Yes / Anything else  - No");
            isVacation = Integer.parseInt(br.readLine()) == 1 ? true : false;

            isr.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


}
