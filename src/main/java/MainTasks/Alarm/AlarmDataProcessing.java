package MainTasks.Alarm;

public class AlarmDataProcessing {

    /**
     * Checks the alarm time basing on number of day, and boolean, which represents if now is a vacation
     *
     * @param day integer from 0 to 6 which represents days of week, starting from Sunday
     * @param isVacation boolean which is used as flag, to tell program, if it's a vacation now
     * @return String which contains alarm time
     */
    public static String alarmTime(int day, boolean isVacation) {

        String Alarm;

        Alarm = isVacation && (day == 0 || day == 6) ? "Alarm off"
                : !isVacation && (day > 0 && day < 6) ? "Alarm time: 07:00"
                : "Alarm time: 10:00";

        return Alarm;
    } // alarmTime(...) end
} // class AlarmDataProcessing end
